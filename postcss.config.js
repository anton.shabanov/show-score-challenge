module.exports = ({ file }) => ({
    parser: file.extname === '.sss' ? 'sugarss' : false,
    plugins: {
      autoprefixer: {},
      'postcss-nested': {},
      'postcss-hexrgba': {},
      'postcss-simple-vars': {},
      'postcss-mixins': {},
    },
});
