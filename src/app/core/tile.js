import { COLOR } from './constants';

// simple tile builder
export const buildTile = (i, j, args = {}) => ({ i, j, index: `{${i}:${j}}`, color: COLOR.white, ...args });

// fill tile
export const fillTile = (tile, color) => buildTile(tile.i, tile.j, { color });
