// state machine
export const state = (initialState => {
  let innerState = null;
  return (newState = null) => {
    return (innerState = newState || innerState || initialState);
  };
})([]);

// simple state injector
export const injectState = func => (...args) => {
  return func.call(null, getState(), ...args);
};

// returns current game state
export const getState = () => [].concat(state());
