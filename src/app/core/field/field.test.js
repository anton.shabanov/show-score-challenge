import {
  field,
  tile,
  tileAbove,
  tileBelow,
  couldClearTile,
  couldFillTile,
  clickField,
  clearFill,
  rainFill,
} from './field';
import { fillTile } from '../tile';
import { COLOR } from '../constants';

describe('field', () => {
  let f = null;
  const N = 10,
    M = 15;

  beforeEach(() => {
    f = field(N, M);
  });

  describe('creation', () => {
    test('should be able to set up new one', () => {
      expect(f).not.toBe(null);
    });

    test('should be able to set up new one with right size', () => {
      expect(f.length).toBe(N);
    });

    test('should be able to set up new one with right row size', () => {
      expect(f[0].length).toBe(M);
    });
  });

  describe('tile', () => {
    test('should be able to get tile by coords', () => {
      const t = tile(f, 8, 1);
      expect(t).not.toBe(null);
    });

    test('should be object', () => {
      const t = tile(f, 0, 0);
      expect(typeof t).toBe('object');
    });

    test('should be object with right coords', () => {
      const i = 1,
        j = 5;
      const t = tile(f, i, j);
      expect(t.i).toBe(i);
      expect(t.j).toBe(j);
    });
  });

  describe('tileAbove', () => {
    test('should be able to get tile above by coords', () => {
      const t = tile(f, 8, 1);
      const above = tileAbove(f, t.i, t.j);
      expect(above.i).toBe(t.i - 1);
      expect(above.j).toBe(t.j);
    });

    test('should be able to get null if we can not get above one', () => {
      const t = tile(f, 0, 0);
      const above = tileAbove(f, t.i, t.j);
      expect(above).toBe(null);
    });
  });

  describe('tileBelow', () => {
    test('should be able to get tile below by coords', () => {
      const t = tile(f, 8, 1);
      const above = tileBelow(f, t.i, t.j);
      expect(above.i).toBe(t.i + 1);
      expect(above.j).toBe(t.j);
    });

    test('should be below to get null if we can not get above one', () => {
      const t = tile(f, N - 1, 0);
      const above = tileBelow(f, t.i, t.j);
      expect(above).toBe(null);
    });
  });

  describe('couldClearTile', () => {
    test('should not be able to clear white tile', () => {
      const t = tile(f, N - 1, 0);
      expect(couldClearTile(f, t.i, t.j)).toBeFalsy();
    });

    test('should be able to clear black "top" tile', () => {
      const newF = [].concat(f);
      const t1 = tile(newF, N - 1, 0);
      const t2 = tile(newF, t1.i - 1, 0);
      newF[t1.i][t1.j] = fillTile(t1, COLOR.black);
      newF[t2.i][t2.j] = fillTile(t2, COLOR.black);
      expect(couldClearTile(f, t2.i, t2.j)).toBeTruthy();
    });

    test('should not be able to clear black "non-top" tile', () => {
      const newF = [].concat(f);
      const t1 = tile(newF, N - 1, 0);
      const t2 = tile(newF, t1.i - 1, 0);
      newF[t1.i][t1.j] = fillTile(t1, COLOR.black);
      newF[t2.i][t2.j] = fillTile(t2, COLOR.black);
      expect(couldClearTile(f, t1.i, t1.j)).toBeFalsy();
    });
  });

  describe('couldFillTile', () => {
    test('should not be able to fill "floating" tile', () => {
      const t = tile(f, 0, 0);
      expect(couldFillTile(f, t.i, t.j)).toBeFalsy();
    });

    test('should be able to fill "top floating" tile', () => {
      const t = tile(f, N - 1, M - 1);
      expect(couldFillTile(f, t.i, t.j)).toBeTruthy();
    });

    test('should be able to fill "top floating" tile', () => {
      const newF = [].concat(f);
      const t1 = tile(newF, N - 1, 0);
      const t2 = tile(newF, t1.i - 1, 0);
      expect(couldFillTile(f, t1.i, t1.j)).toBeTruthy();
      expect(couldFillTile(f, t2.i, t2.j)).toBeFalsy();
    });
  });

  describe('clickField', () => {
    test('should be able to click white bottom tile', () => {
      const t = tile(f, N - 1, M - 2);
      const newF = clickField(f, t.i, t.j);
      const nT = tile(newF, t.i, t.j);
      expect(nT.color).toBe(COLOR.black);
    });

    test('should not be able to click white top tile', () => {
      const t = tile(f, 0, M - 2);
      const newF = clickField(f, t.i, t.j);
      const nT = tile(newF, t.i, t.j);
      expect(nT.color).toBe(COLOR.white);
    });

    test('should be able to click black bottom tile', () => {
      const t = tile(f, N - 1, M - 2);
      const newF = clickField(f, t.i, t.j);
      newF[t.i][t.j] = fillTile(t, COLOR.black);
      expect(t.color).toBe(COLOR.white);
    });

    test('should be able to click black bottom tile with black top above', () => {
      let newF = [].concat(f);
      let t1 = tile(newF, N - 1, 0);
      const t2 = tile(newF, t1.i - 1, 0);
      newF[t1.i][t1.j] = fillTile(t1, COLOR.black);
      newF[t2.i][t2.j] = fillTile(t2, COLOR.black);
      newF = clickField(newF, t1.i, t1.j);
      t1 = tile(newF, N - 1, 0);
      expect(t1.color).toBe(COLOR.black);
    });

    test('should be able to click black top tile', () => {
      let newF = [].concat(f);
      const t1 = tile(newF, N - 1, 0);
      let t2 = tile(newF, t1.i - 1, 0);
      newF[t1.i][t1.j] = fillTile(t1, COLOR.black);
      newF[t2.i][t2.j] = fillTile(t2, COLOR.black);
      newF = clickField(newF, t2.i, t2.j);
      t2 = tile(newF, t1.i - 1, 0);
      expect(t2.color).toBe(COLOR.white);
    });
  });

  describe('clearFill', () => {
    test('should be able to clear field from blue tiles', () => {
      let newF = [].concat(f);
      const t = tile(newF, N - 1, 0);
      newF[t.i][t.j] = fillTile(t, COLOR.blue);
      newF = clearFill(newF);
      const blueRows = clearFill(newF).filter(
        row => row.map(t => t.color).indexOf(COLOR.blue) !== -1,
      );
      expect(blueRows.length).toBe(0);
    });
  });

  describe('rainFill', () => {
    const blueRows = f => f.filter(row => row.map(t => t.color).indexOf(COLOR.blue) !== -1);

    test('should be able not to fill white rows', () => {
      expect(blueRows(rainFill(f)).length).toBe(0);
    });

    test('should be able not to fill non pair rows', () => {
      let newF = clickField(f, N - 1, 0);
      newF = clickField(f, N - 2, 0);
      expect(blueRows(rainFill(newF)).length).toBe(0);
    });

    test('should be able to fill pair rows', () => {
      let newF = clickField(f, N - 1, 0);
      newF = clickField(f, N - 1, 2);
      expect(blueRows(rainFill(newF)).length).not.toBe(0);
    });

    test('should be able to fill multiple pair rows', () => {
      let newF = clickField(f, N - 1, 0);
      newF = clickField(f, N - 1, M - 1);
      newF = clickField(f, N - 2, 0);
      newF = clickField(f, N - 2, M - 1);
      expect(blueRows(rainFill(newF)).length).toBe(2);
    });

    // TODO add more rainFill specs...
  });
});
