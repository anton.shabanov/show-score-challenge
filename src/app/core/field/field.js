import { buildTile, fillTile } from '../tile';
import { COLOR } from '../constants';

// simple field builder
export const field = (n, m) => Array(n).fill().map((_, i) => Array(m).fill().map((_, j) => buildTile(i, j)));

// get tile
export const tile = (field, i, j) => field[i][j];

// get tile above
export const tileAbove = (field, i, j) => (i === 0 ? null : field[i - 1][j]);

// get tile below
export const tileBelow = (field, i, j) => (i === field.length - 1 ? null : field[i + 1][j]);

// check if we could clear the tile
export const couldClearTile = (field, i, j) => {
  // we can't clear white one since it's already cleared
  const t = tile(field, i, j);
  if (t.color === COLOR.white) return false;

  // we can't clear it if we have a black one above
  const tA = tileAbove(field, i, j);
  return !(tA !== null && tA.color === COLOR.black);
};

// check if we could fill in the tile
export const couldFillTile = (field, i, j) => {
  // we can't fill in black one
  const t = tile(field, i, j);
  if (t.color === COLOR.black) return false;

  const tB = tileBelow(field, i, j);
  return !(tB !== null && tB.color !== COLOR.black);
};

export const clickField = (field, i, j) => {
  const currentField = [].concat(field);
  const t = tile(currentField, i, j);

  // if we could clear it now, let's clear,
  // otherwise let's try to fill
  if (couldClearTile(currentField, i, j)) {
    currentField[i][j] = fillTile(t, COLOR.white);
  } else if (couldFillTile(currentField, i, j)) {
    currentField[i][j] = fillTile(t, COLOR.black);
  }

  return currentField;
};

// clear field from blue tiles
export const clearFill = field => {
  return field.map(row =>
    row.map(tile => {
      const color = tile.color === COLOR.blue ? COLOR.white : tile.color;
      return { ...tile, color };
    }),
  );
};

// fill the field with "rain"
export const rainFill = (field, i, j) => {
  if (i < 0) return field;

  const currentField = [].concat(field);
  const x = i == null ? currentField.length - 1 : i;
  const y = j == null ? 0 : j;
  const row = currentField[x];

  let pair = [null, null];
  let shouldGoUp = false;

  for (let idx = y; idx < row.length; idx++) {
    const item = row[idx];
    if (item.color === COLOR.black) {
      if (pair[0] === null) {
        pair[0] = item;
      } else {
        pair[1] = item;

        // fill pairs
        const [one, two] = pair;
        const startFrom = one.j + 1;
        const endWith = two.j;

        // let's try to fill somethings if we really could fill it
        if (startFrom < endWith) {
          for (let z = startFrom; z < endWith; z++) {
            const tile = currentField[one.i][z];
            currentField[one.i][z] = fillTile(tile, COLOR.blue);
          }

          // once we filled up something on this level we should try another one, otherwise no need to do that
          shouldGoUp = true;
        }

        // start with the last found tile as the new starting edge
        pair = [item, null];
      }
    }
  }

  // let's just return if there is no fills on this level
  return shouldGoUp ? rainFill(currentField, x - 1, 0) : currentField;
};
