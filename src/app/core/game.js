import { state, injectState } from './state';
import { field, clearFill, rainFill, clickField } from './field';

export default function game(n, m) {
  // init method
  const init = () => state(field(n, m));

  // returns current game state
  const getState = () => [].concat(state());

  // on i, j item click
  const click = (currentState, i, j) => state(clickField(currentState, i, j));

  // fill the field with "rain"
  const fill = currentState => state(rainFill(currentState));

  const clear = currentState => state(clearFill(currentState));

  // public interface
  const contract = {
    rebuild: init,
    getState,
    click: injectState(click),
    fill: injectState(fill),
    clear: injectState(clear),
  };

  // init and get the contract to user
  return (() => {
    init();
    return contract;
  })();
}
