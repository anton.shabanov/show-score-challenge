import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import Wrapper from '../../components/Wrapper';
import Header from '../../components/Header';
import Field from '../../components/Field';
import Button from '../../components/Button';
import styles from './styles.sss';

@observer
export default class App extends Component {
  static propTypes = {
    store: PropTypes.shape({}).isRequired,
  };

  onTileClick = tile => {
    const { store } = this.props;
    store.select(tile);
  };

  reset = () => {
    const { store } = this.props;
    store.reset();
  };

  fill = () => {
    const { store } = this.props;
    store.fill();
  };

  render() {
    const {
      store: { state },
    } = this.props;
    return (
      <div className={styles.app}>
        <Wrapper>
          <div className={styles.appLogo}>
            <Header />
          </div>
          <div className={styles.appField}>
            <Field field={state} onTileClick={this.onTileClick} />
            { /* TODO split into actions panel... */ }
            <div className={styles.appButtons}>
              <div className={styles.appButton}>
                <Button onClick={this.reset} invert>
                  Reset
                </Button>
              </div>
              <div className={styles.appButton}>
                <Button onClick={this.fill}>Run</Button>
              </div>
            </div>
          </div>
        </Wrapper>
      </div>
    );
  }
}
