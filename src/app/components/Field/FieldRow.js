import React from 'react';
import PropTypes from 'prop-types';
import Tile from '../Tile';
import styles from './styles.sss';

export default function FieldRow({ row, onTileClick }) {
  return (
    <div className={styles.row}>
      {row.map(tile => (
        <Tile key={tile.index} tile={tile} onClick={onTileClick} />
      ))}
    </div>
  );
}

FieldRow.propTypes = {
  row: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onTileClick: PropTypes.func.isRequired,
};
