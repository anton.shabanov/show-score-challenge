import React from 'react';
import PropTypes from 'prop-types';
import FieldRow from './FieldRow';
import styles from './styles.sss';

export default function Field({ field, onTileClick }) {
  return (
    <div className={styles.field}>
      {field.map((row, index) => (
        <FieldRow key={index} row={row} onTileClick={onTileClick} />
      ))}
    </div>
  );
}

Field.propTypes = {
  field: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({}))).isRequired,
  onTileClick: PropTypes.func.isRequired,
};
