import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './styles.sss';

export default function Button({ children, onClick, invert, ...rest }) {
  const buttonStyles = cn(styles.button, { [styles.buttonInvert]: !!invert });
  return (
    <button type="button" {...rest} onClick={onClick} className={buttonStyles}>
      {children}
    </button>
  );
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
  invert: PropTypes.bool,
};

Button.defaultProps = {
  invert: false,
};
