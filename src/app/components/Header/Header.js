import React from 'react';
import logo from './images/header.png';
import styles from './styles.sss';

export default function Header() {
  return (
    <div className={styles.header}>
      <img src={logo} alt="" />
    </div>
  );
}
