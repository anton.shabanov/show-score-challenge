import React from 'react';
import styles from './styles.sss';
import PropTypes from "prop-types";

export default function Wrapper({ children }) {
  return <div className={styles.wrapper}>{children}</div>;
}

Wrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
};
