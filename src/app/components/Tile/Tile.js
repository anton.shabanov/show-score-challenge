import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './styles.sss';

export default function Tile({ tile, onClick }) {
  const tileStyles = cn(styles.tile, {
    [styles.tileBlue]: tile.color === 'blue',
    [styles.tileBlack]: tile.color === 'black',
  });
  return <div className={tileStyles} onClick={() => onClick(tile)} />;
}

Tile.propTypes = {
  tile: PropTypes.shape({}).isRequired,
  onClick: PropTypes.func.isRequired,
};
