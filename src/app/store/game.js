import { observable } from 'mobx';
import g from '../core/game';

const game = (window.game = g(6, 12));

export class Game {
  shouldClear = false;
  @observable
  state = game.getState();

  select = tile => {
    if (this.shouldClear) {
      game.clear();
      this.shouldClear = false;
    }
    this.state = game.click(tile.i, tile.j);
  };

  reset = () => {
    this.shouldClear = false;
    this.state = game.rebuild();
  };

  fill = () => {
    this.shouldClear = true;
    this.state = game.fill();
  };
}
