import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/pages/App';
import store from './app/store';
import './app/global/styles.sss';

// https://bitbucket.org/svyatov/show-score-rain-challenge/src/master/

ReactDOM.render(<App store={store} />, document.getElementById('app'));
