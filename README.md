# Start

To setup the app, simply do the following commands:

1. `$ cd working-dir`
2. `$ yarn # or npm i`
3. `$ yarn dev # to start dev server`
4. `$ yarn build # to rebuild the app, see the output inside /dist folder`
5. `$ yarn tests # to run jest tests`
